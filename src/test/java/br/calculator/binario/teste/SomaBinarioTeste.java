/**
 * 
 */
package br.calculator.binario.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.binario.ISomaBinario;
import br.calculator.binario.SomaBinario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 4.0
 */
public class SomaBinarioTeste {
	final ISomaBinario somaBinario = new SomaBinario();

	@Test
	public void deveSomarBinarioInt() {
		final String resultado = somaBinario.somaBinario(1, 1);

		Assert.assertEquals("10", resultado);
	}

	@Test
	public void deveSomarBinarioString() {
		final String teste1 = somaBinario.somaBinario("0", "0");
		final String teste2 = somaBinario.somaBinario("10", "1");
		final String teste3 = somaBinario.somaBinario("11", "1");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("11", teste2);
		Assert.assertEquals("100", teste3);
	}
}