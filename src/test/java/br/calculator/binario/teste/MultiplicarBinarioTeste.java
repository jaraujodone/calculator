/**
 * 
 */
package br.calculator.binario.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.binario.IMultiplicarBinario;
import br.calculator.binario.MultiplicarBinario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 4.0
 */
public class MultiplicarBinarioTeste {
	final IMultiplicarBinario multiplicaBinario = new MultiplicarBinario();
	
	@Test
	public void devemMultiplicarBinarioInt() {
		final String resultado = multiplicaBinario.multiplicarBinario(11, 1);

		Assert.assertEquals("1011", resultado);
	}

	@Test
	public void deveMultiplicarBinarioString() {
		final String teste1 = multiplicaBinario.multiplicarBinario("0", "0");
		final String teste2 = multiplicaBinario.multiplicarBinario("10", "1");
		final String teste3 = multiplicaBinario.multiplicarBinario("11", "1");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("10", teste2);
		Assert.assertEquals("11", teste3);
	}
}