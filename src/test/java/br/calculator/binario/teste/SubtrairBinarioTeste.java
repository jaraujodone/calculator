/**
 * 
 */
package br.calculator.binario.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.binario.ISubtrairBinario;
import br.calculator.binario.SubtrairBinario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 4.0
 */
public class SubtrairBinarioTeste {
	final ISubtrairBinario subtrairBinario = new SubtrairBinario();
	
	@Test
	public void deveSubtrairBinarioInt() {
		int numero1 = Integer.parseInt("1", 2);
		int numero2 = Integer.parseInt("-1", 2);
		
		final String resultado = subtrairBinario.subtrairBinario(numero1, numero2);
		
		Assert.assertEquals("10", resultado);
	}
	
	@Test
	public void deveSomarBinarioString() {		
		final String teste1 = subtrairBinario.subtrairBinario("0", "0");
		final String teste2 = subtrairBinario.subtrairBinario("10", "1");
		final String teste3 = subtrairBinario.subtrairBinario("11", "1");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("1", teste2);
		Assert.assertEquals("10", teste3);
	}
}