package br.calculator.binario.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.binario.BinarioToDecimal;
import br.calculator.binario.IBinarioToDecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class BinarioToDecimalTeste {

	final IBinarioToDecimal binarioParaDecimal = new BinarioToDecimal();
	
	@Test
	public void deveConverterBinarioEmDecimal() {		
		final int teste1 = binarioParaDecimal.binarioToDecimal("0");
		final int teste2 = binarioParaDecimal.binarioToDecimal("10");
		final int teste3 = binarioParaDecimal.binarioToDecimal("1000");

		Assert.assertEquals(0, teste1);
		Assert.assertEquals(2, teste2);
		Assert.assertEquals(8, teste3);
	}
}