package br.calculator.binario.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.binario.BinarioToHexadecimal;
import br.calculator.binario.IBinarioToHexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class BinarioToHexadecimalTeste {

	final IBinarioToHexadecimal binarioParaHexadecimal = new BinarioToHexadecimal();
	
	@Test
	public void deveConverterBinarioEmHexadecimal() {
		final String teste1 = binarioParaHexadecimal.binarioToHexadecimal("0");
		final String teste2 = binarioParaHexadecimal.binarioToHexadecimal("10");
		final String teste3 = binarioParaHexadecimal.binarioToHexadecimal("1100");

		Assert.assertEquals("0", teste1.toUpperCase());
		Assert.assertEquals("2", teste2.toUpperCase());
		Assert.assertEquals("C", teste3.toUpperCase());
	}
}