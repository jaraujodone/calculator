/**
 * 
 */
package br.calculator.binario.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.binario.DividirBinario;
import br.calculator.binario.IDividirBinario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 4.0
 */
public class DividirBinarioTeste {
	final IDividirBinario dividirBinario = new DividirBinario();
	
	@Test
	public void deveDividirBinarioInt() {
		final String resultado = dividirBinario.dividirBinario(1, 1);

		Assert.assertEquals("1", resultado);
	}

	@Test
	public void deveDividirBinarioString() {
		final String teste1 = dividirBinario.dividirBinario("10", "1");
		final String teste2 = dividirBinario.dividirBinario("11", "1");

		Assert.assertEquals("10", teste1);
		Assert.assertEquals("11", teste2);
	}
	
	@Test(expected = ArithmeticException.class)
	public void deveDividirBinarioZero() {
		dividirBinario.dividirBinario("0", "0");
	}
}