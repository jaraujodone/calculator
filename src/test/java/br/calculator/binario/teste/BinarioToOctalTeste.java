package br.calculator.binario.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.binario.BinarioToOctal;
import br.calculator.binario.IBinarioToOctal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class BinarioToOctalTeste {

	final IBinarioToOctal binarioParaOctal = new BinarioToOctal();
	
	@Test
	public void deveConverterBinarioEmOctal() {		
		final String teste1 = binarioParaOctal.binarioToOctal("0");
		final String teste2 = binarioParaOctal.binarioToOctal("10");
		final String teste3 = binarioParaOctal.binarioToOctal("1000");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("2", teste2);
		Assert.assertEquals("10", teste3);
	}
}