package br.calculator.decimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.decimal.DecimalToHexadecimal;
import br.calculator.decimal.IDecimalToHexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class DecimalToHexadecimalTeste {

	final IDecimalToHexadecimal decimalParaHexadecimal = new DecimalToHexadecimal();

	@Test
	public void deveConverterDecimalEmHexadecimal() {
		final String teste1 = decimalParaHexadecimal.decimalToHexadecimal(0);
		final String teste2 = decimalParaHexadecimal.decimalToHexadecimal(2);
		final String teste3 = decimalParaHexadecimal.decimalToHexadecimal(8);
		final String teste4 = decimalParaHexadecimal.decimalToHexadecimal(15);

		Assert.assertEquals("0", teste1.toUpperCase());
		Assert.assertEquals("2", teste2.toUpperCase());
		Assert.assertEquals("8", teste3.toUpperCase());
		Assert.assertEquals("F", teste4.toUpperCase());
	}
}