package br.calculator.decimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.decimal.DecimalToBinario;
import br.calculator.decimal.IDecimalToBinario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class DecimalToBinarioTeste {

	final IDecimalToBinario decimalParaBinario = new DecimalToBinario();

	@Test
	public void deveConverterDecimalEmBinario() {
		final String teste1 = decimalParaBinario.decimalToBinario(0);
		final String teste2 = decimalParaBinario.decimalToBinario(2);
		final String teste3 = decimalParaBinario.decimalToBinario(8);

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("10", teste2);
		Assert.assertEquals("1000", teste3);
	}
}