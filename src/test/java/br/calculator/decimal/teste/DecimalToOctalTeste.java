package br.calculator.decimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.decimal.DecimalToOctal;
import br.calculator.decimal.IDecimalToOctal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class DecimalToOctalTeste {

	final IDecimalToOctal decimalParaOctal = new DecimalToOctal();
	
	@Test
	public void deveConverterDecimalEmOctal() {
		final String teste1 = decimalParaOctal.decimalToOctal(0);
		final String teste2 = decimalParaOctal.decimalToOctal(2);
		final String teste3 = decimalParaOctal.decimalToOctal(8);
		final String teste4 = decimalParaOctal.decimalToOctal(15);

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("2", teste2);
		Assert.assertEquals("10", teste3);
		Assert.assertEquals("17", teste4);
	}
}