package br.calculator.hexadecimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.hexadecimal.ISubtrairHexadecimal;
import br.calculator.hexadecimal.SubtrairHexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 8.0
 */
public class SubtrairHexadecimalTeste {
	
	final ISubtrairHexadecimal subtrairHexadecimal = new SubtrairHexadecimal();
	
	@Test
	public void deveSubtrairHexadecimalInt() {
		final String resultado = subtrairHexadecimal.subtraiHexadecimal(1, 1);
		
		Assert.assertEquals("0", resultado);
	}

	@Test
	public void deveSubtrairHexadecimalString() {
		final String teste1 = subtrairHexadecimal.subtraiHexadecimal("0", "0");
		final String teste2 = subtrairHexadecimal.subtraiHexadecimal("2", "1");
		final String teste3 = subtrairHexadecimal.subtraiHexadecimal("B", "1");

		Assert.assertEquals("0", teste1.toUpperCase());
		Assert.assertEquals("1", teste2.toUpperCase());
		Assert.assertEquals("A", teste3.toUpperCase());
	}
}