package br.calculator.hexadecimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.hexadecimal.ISomaHexadecimal;
import br.calculator.hexadecimal.SomaHexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 8.0
 */
public class SomaHexadecimalTeste {
	
	final ISomaHexadecimal somaHexadecimal = new SomaHexadecimal();
	
	@Test
	public void deveSomarHexadecimalInt() {
		final String resultado = somaHexadecimal.somaHexadecimal("9", "1");
		
		Assert.assertEquals("A", resultado.toUpperCase());
	}

	@Test
	public void deveSomarHexadecimalString() {
		final String teste1 = somaHexadecimal.somaHexadecimal("0", "0");
		final String teste2 = somaHexadecimal.somaHexadecimal("2", "1");
		final String teste3 = somaHexadecimal.somaHexadecimal("9", "1");

		Assert.assertEquals("0", teste1.toUpperCase());
		Assert.assertEquals("3", teste2.toUpperCase());
		Assert.assertEquals("A", teste3.toUpperCase());
	}
}