package br.calculator.hexadecimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.hexadecimal.HexadecimalToOctal;
import br.calculator.hexadecimal.IHexadecimalToOctal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class HexadecimalToOctalTeste {
	final IHexadecimalToOctal hexaParaOctal = new HexadecimalToOctal();
	
	@Test
	public void deveConverterHexadecimalEmOctal() {
		final String teste1 = hexaParaOctal.hexadecimalToOctal("0");
		final String teste2 = hexaParaOctal.hexadecimalToOctal("7");
		final String teste3 = hexaParaOctal.hexadecimalToOctal("F");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("7", teste2);
		Assert.assertEquals("17", teste3);
	}
}