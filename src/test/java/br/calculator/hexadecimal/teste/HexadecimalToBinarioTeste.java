package br.calculator.hexadecimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.hexadecimal.HexadecimalToBinario;
import br.calculator.hexadecimal.IHexadecimalToBinario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class HexadecimalToBinarioTeste {
	final IHexadecimalToBinario hexaParaBinario = new HexadecimalToBinario();
	
	@Test
	public void deveConverterHexadecimalEmBinario() {
		final String teste1 = hexaParaBinario.hexadecimalToBinario("0");
		final String teste2 = hexaParaBinario.hexadecimalToBinario("7");
		final String teste3 = hexaParaBinario.hexadecimalToBinario("F");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("111", teste2);
		Assert.assertEquals("1111", teste3);
	}
}