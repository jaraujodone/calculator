package br.calculator.hexadecimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.hexadecimal.HexadecimalToDecimal;
import br.calculator.hexadecimal.IHexadecimalToDecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class HexadecimalToDecimalTeste {
	final IHexadecimalToDecimal hexaParaDecimal = new HexadecimalToDecimal();
	
	@Test
	public void deveConverterHexadecimalEmDecimal() {
		final int teste1 = hexaParaDecimal.hexadecimalToDecimal("0");
		final int teste2 = hexaParaDecimal.hexadecimalToDecimal("7");
		final int teste3 = hexaParaDecimal.hexadecimalToDecimal("F");

		Assert.assertEquals(0, teste1);
		Assert.assertEquals(7, teste2);
		Assert.assertEquals(15, teste3);
	}
}