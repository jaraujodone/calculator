package br.calculator.hexadecimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.hexadecimal.DividirHexadecimal;
import br.calculator.hexadecimal.IDividirHexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 8.0
 */
public class DividirHexadecimalTeste {
	
	final IDividirHexadecimal dividirHexadecimal = new DividirHexadecimal();
	
	@Test
	public void deveDividirHexadecimalInt() {
		final String resultado = dividirHexadecimal.dividiHexadecimal(1, 1);
		
		Assert.assertEquals("1", resultado);
	}

	@Test
	public void deveDividirHexadecimalString() {
		final String teste1 = dividirHexadecimal.dividiHexadecimal("2", "1");
		final String teste2 = dividirHexadecimal.dividiHexadecimal("A", "2");

		Assert.assertEquals("2", teste1);
		Assert.assertEquals("5", teste2);
	}
	
	@Test(expected = ArithmeticException.class)
	public void deveDividirHexadecimalZero() {
		dividirHexadecimal.dividiHexadecimal("0", "0");
	}
}