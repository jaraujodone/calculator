package br.calculator.hexadecimal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.hexadecimal.IMultiplicarHexadecimal;
import br.calculator.hexadecimal.MultiplicarHexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 8.0
 */
public class MultiplicarHexadecimalTeste {
	
	final IMultiplicarHexadecimal multiplicaHexadecimal = new MultiplicarHexadecimal();
	
	@Test
	public void deveMultiplicarHexadecimalInt() {
		final String resultado = multiplicaHexadecimal.multiplicaHexadecimal(1, 1);
		
		Assert.assertEquals("1", resultado);
	}

	@Test
	public void deveMultiplicarHexadecimalString() {
		final String teste1 = multiplicaHexadecimal.multiplicaHexadecimal("0", "0");
		final String teste2 = multiplicaHexadecimal.multiplicaHexadecimal("2", "1");
		final String teste3 = multiplicaHexadecimal.multiplicaHexadecimal("B", "1");

		Assert.assertEquals("0", teste1.toUpperCase());
		Assert.assertEquals("2", teste2.toUpperCase());
		Assert.assertEquals("B", teste3.toUpperCase());
	}
}