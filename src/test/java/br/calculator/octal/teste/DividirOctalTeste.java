package br.calculator.octal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.octal.DividirOctal;
import br.calculator.octal.IDividirOctal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 10.0
 */
public class DividirOctalTeste {
	
	final IDividirOctal dividirOctal = new DividirOctal();
	
	@Test
	public void deveDividirOctalInt() {
		final String resultado = dividirOctal.dividiOctal(1, 1);
		
		Assert.assertEquals("1", resultado);
	}

	@Test
	public void deveDividirOctalString() {
		final String teste1 = dividirOctal.dividiOctal("2", "1");
		final String teste2 = dividirOctal.dividiOctal("10", "2");

		Assert.assertEquals("2", teste1);
		Assert.assertEquals("4", teste2);
	}
	
	@Test(expected = ArithmeticException.class)
	public void deveDividirOctalZero() {
		dividirOctal.dividiOctal("0", "0");
	}
}