package br.calculator.octal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.octal.ISubtrairOctal;
import br.calculator.octal.SubtrairOctal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 10.0
 */
public class SubtrairOctalTeste {
	
	final ISubtrairOctal subtrairOctal = new SubtrairOctal();
	
	@Test
	public void deveSubtrairHexadecimalInt() {
		final String resultado = subtrairOctal.subtraiOctal(1, 1);
		
		Assert.assertEquals("0", resultado);
	}

	@Test
	public void deveSubtrairHexadecimalString() {
		final String teste1 = subtrairOctal.subtraiOctal("0", "0");
		final String teste2 = subtrairOctal.subtraiOctal("2", "1");
		final String teste3 = subtrairOctal.subtraiOctal("7", "1");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("1", teste2);
		Assert.assertEquals("6", teste3);
	}
}