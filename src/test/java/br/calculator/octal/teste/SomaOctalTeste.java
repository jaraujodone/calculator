package br.calculator.octal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.octal.ISomaOctal;
import br.calculator.octal.SomaOctal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 10.0
 */
public class SomaOctalTeste {
	
	final ISomaOctal somaOctal = new SomaOctal();
	
	@Test
	public void deveSomarHexadecimalInt() {
		final String resultado = somaOctal.somaOctal(7, 1);
		
		Assert.assertEquals("10", resultado);
	}

	@Test
	public void deveSomarHexadecimalString() {
		final String teste1 = somaOctal.somaOctal("0", "0");
		final String teste2 = somaOctal.somaOctal("2", "1");
		final String teste3 = somaOctal.somaOctal("7", "1");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("3", teste2);
		Assert.assertEquals("10", teste3);
	}
}