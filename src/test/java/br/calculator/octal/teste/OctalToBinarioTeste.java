package br.calculator.octal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.octal.IOctalToBinario;
import br.calculator.octal.OctalToBinario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class OctalToBinarioTeste {
	final IOctalToBinario octalParaBinario = new OctalToBinario();
	
	@Test
	public void deveConverterOctalEmBinario() {
		final String teste1 = octalParaBinario.octalToBinario("0");
		final String teste2 = octalParaBinario.octalToBinario("7");
		final String teste3 = octalParaBinario.octalToBinario("10");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("111", teste2);
		Assert.assertEquals("1000", teste3);
	}
}