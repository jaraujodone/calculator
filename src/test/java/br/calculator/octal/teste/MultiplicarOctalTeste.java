package br.calculator.octal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.octal.IMultiplicarOctal;
import br.calculator.octal.MultiplicarOctal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 10.0
 */
public class MultiplicarOctalTeste {
	
	final IMultiplicarOctal multiplicaOctal = new MultiplicarOctal();
	
	@Test
	public void deveMultiplicarOctalInt() {
		final String resultado = multiplicaOctal.multiplicaOctal(7, 1);
		
		Assert.assertEquals("7", resultado);
	}

	@Test
	public void deveMultiplicarOctalString() {
		final String teste1 = multiplicaOctal.multiplicaOctal("0", "0");
		final String teste2 = multiplicaOctal.multiplicaOctal("2", "1");
		final String teste3 = multiplicaOctal.multiplicaOctal("7", "1");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("2", teste2);
		Assert.assertEquals("7", teste3);
	}
}