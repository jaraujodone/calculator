package br.calculator.octal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.octal.IOctalToDecimal;
import br.calculator.octal.OctalToDecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class OctalToDecimalTeste {
	final IOctalToDecimal octalParaDecimal = new OctalToDecimal();
	
	@Test
	public void deveConverterOctalEmDecimal() {
		final int teste1 = octalParaDecimal.octalToDecimal("0");
		final int teste2 = octalParaDecimal.octalToDecimal("7");
		final int teste3 = octalParaDecimal.octalToDecimal("10");

		Assert.assertEquals(0, teste1);
		Assert.assertEquals(7, teste2);
		Assert.assertEquals(8, teste3);
	}
}