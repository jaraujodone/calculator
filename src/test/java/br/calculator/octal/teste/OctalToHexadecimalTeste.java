package br.calculator.octal.teste;

import junit.framework.Assert;

import org.junit.Test;

import br.calculator.octal.IOctalToHexadecimal;
import br.calculator.octal.OctalToHexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 12.0
 */
public class OctalToHexadecimalTeste {
	final IOctalToHexadecimal octalParaHexadecimal = new OctalToHexadecimal();
	
	@Test
	public void deveConverterOctalEmHexadecimal() {
		final String teste1 = octalParaHexadecimal.octalToHexadecimal("0");
		final String teste2 = octalParaHexadecimal.octalToHexadecimal("7");
		final String teste3 = octalParaHexadecimal.octalToHexadecimal("10");

		Assert.assertEquals("0", teste1);
		Assert.assertEquals("7", teste2);
		Assert.assertEquals("8", teste3);
	}
}