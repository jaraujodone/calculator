/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.matriz.ISomaMatriz;
import br.calculator.matriz.MatrizException;
import br.calculator.matriz.SomaMatriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 2.0
 */
public class SomaMatrizTeste {

	final ISomaMatriz soma = new SomaMatriz();

	@Test
	public void deveSomaMatrizInt() throws MatrizException {
		final int[][] teste1Mat1 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1Mat2 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1ResultadoReal = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1ResultadoMetodo = soma.somaMatriz(teste1Mat1, teste1Mat2);

		//Teste 1
		Assert.assertArrayEquals(teste1ResultadoReal, teste1ResultadoMetodo);
		
		final int[][] teste3Mat1 = new int[][] {{-1, 0, -4}, {-2, 0, -3}};

		final int[][] teste3Mat2 = new int[][] {{0, -5, 0}, {0, -6, 0}};

		final int[][] teste3ResultadoReal = new int[][] {{-1, -5, -4}, {-2, -6, -3}};

		final int[][] teste3ResultadoMetodo = soma.somaMatriz(teste3Mat1, teste3Mat2);

		//Teste 3
		Assert.assertArrayEquals(teste3ResultadoReal, teste3ResultadoMetodo);
		
		final int[][] teste4Mat1 = new int[][] {{-1, -5, -4}, {0, 0, 0}};

		final int[][] teste4Mat2 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste4ResultadoReal = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste4ResultadoMetodo = soma.somaMatriz(teste4Mat1, teste4Mat2);

		//Teste 4
		Assert.assertArrayEquals(teste4ResultadoReal, teste4ResultadoMetodo);
	}

	@Test
	public void deveSomaMatrizLong() throws MatrizException {
		final long[][] mat1 = new long[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final long[][] mat2 = new long[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final long[][] resultadoReal = new long[][] {{2000, 4000, 6000}, {8000, 10000, 12000}, {14000, 16000, 18000}};

		final long[][] resultadoMetodo = soma.somaMatriz(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveSomaMatrizFloat() throws MatrizException {
		final float[][] mat1 = new float[][] {{1.1f, 2.2f, 3.3f}, {4.4f, 5.5f, 6.6f}, {7.7f, 8.8f, 9.9f}};

		final float[][] mat2 = new float[][] {{1.1f, 2.2f, 3.3f}, {4.4f, 5.5f, 6.6f}, {7.7f, 8.8f, 9.9f}};

		final float[][] resultadoReal = new float[][] {{2.2f, 4.4f, 6.6f}, {8.8f, 11, 13.2f}, {15.4f, 17.6f, 19.8f}};

		final float[][] resultadoMetodo = soma.somaMatriz(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveSomaMatrizDouble() throws MatrizException {
		final double[][] mat1 = new double[][] {{1.1, 2.2, 3.3}, {4.4, 5.5, 6.6}, {7.7, 8.8, 9.9}};

		final double[][] mat2 = new double[][] {{1.1, 2.2, 3.3}, {4.4, 5.5, 6.6}, {7.7, 8.8, 9.9}};

		final double[][] resultadoReal = new double[][] {{2.2, 4.4, 6.6}, {8.8, 11, 13.2}, {15.4, 17.6, 19.8}};

		final double[][] resultadoMetodo = soma.somaMatriz(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}
}