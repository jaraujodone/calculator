/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.matriz.IMultiplicacaoDeMatrizes;
import br.calculator.matriz.MatrizException;
import br.calculator.matriz.MultiplicacaoDeMatrizes;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 2.0
 */
public class MultiplicacaoDeMatrizesTeste {

	final IMultiplicacaoDeMatrizes multi = new MultiplicacaoDeMatrizes();

	@Test
	public void deveMultiplicaMatrizesInt() throws MatrizException {
		final int[][] teste1Mat1 = new int[][] {{0, 0, 0}};

		final int[][] teste1Mat2 = new int[][] {{0}, {0}, {0}};

		final int[][] teste1ResultadoReal = new int[][] {{0}};

		final int[][] teste1ResultadoMetodo = multi.multiplicaMatrizes(teste1Mat1, teste1Mat2);

		//Teste 1
		Assert.assertArrayEquals(teste1ResultadoReal, teste1ResultadoMetodo);
		
		final int[][] teste3Mat1 = new int[][] {{-2, -4, -3}};

		final int[][] teste3Mat2 = new int[][] {{-3, -5}, {-2, 0}, {-4, -6}};

		final int[][] teste3ResultadoReal = new int[][] {{18, 36, 27}, {22, 44, 33}};

		final int[][] teste3ResultadoMetodo = multi.multiplicaMatrizes(teste3Mat1, teste3Mat2);

		//Teste 3
		Assert.assertArrayEquals(teste3ResultadoReal, teste3ResultadoMetodo);
		
		final int[][] teste4Mat1 = new int[][] {{3, 4, 5}};

		final int[][] teste4Mat2 = new int[][] {{5}, {7}, {2}};

		final int[][] teste4ResultadoReal = new int[][] {{46, 56, 70}};

		final int[][] teste4ResultadoMetodo = multi.multiplicaMatrizes(teste4Mat1, teste4Mat2);

		//Teste 4
		Assert.assertArrayEquals(teste4ResultadoReal, teste4ResultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizesLong() throws MatrizException {
		final long[][] mat1 = new long[][] {{2, 5, 9}, {3, 6, 8}};

		final long[][] mat2 = new long[][] {{2, 7}, {4, 3}, {5, 2}};

		final long[][] resultadoReal = new long[][] {{69, 47}, {70, 55}};

		final long[][] resultadoMetodo = multi.multiplicaMatrizes(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizesFloat() throws MatrizException {
		final float[][] mat1 = new float[][] {{2, 5, 9}, {3, 6, 8}};

		final float[][] mat2 = new float[][] {{2, 7}, {4, 3}, {5, 2}};

		final float[][] resultadoReal = new float[][] {{69, 47}, {70, 55}};

		final float[][] resultadoMetodo = multi.multiplicaMatrizes(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizesDouble() throws MatrizException {
		final double[][] mat1 = new double[][] {{2, 5, 9}, {3, 6, 8}};

		final double[][] mat2 = new double[][] {{2, 7}, {4, 3}, {5, 2}};

		final double[][] resultadoReal = new double[][] {{69, 47}, {70, 55}};

		final double[][] resultadoMetodo = multi.multiplicaMatrizes(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}
}