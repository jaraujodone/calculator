/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import br.calculator.matriz.EscalonaMatrizGauss;
import br.calculator.matriz.IEscalonaMatrizGauss;
import br.calculator.matriz.MatrizException;

/**
 * @author Jesse A. Done
 *  
 * @since Calculator API 18.0
 */
public class EscalonaMatrizPorGaussTeste {

	final IEscalonaMatrizGauss escalonaMatriz = new EscalonaMatrizGauss();
	
	@Test
	@Ignore
	public void deveEscalonarMatrizOpostaInt() throws MatrizException {
		final int[][] mat1 = new int[][] {{2, 3, -1}, {4, 4, -3}, {2, -3, 1}};

		final float[][] resultadoReal = new float[][] {{2, 3, -1}, {0, -2, -1}, {0, 0, 8}};

		final float[][] resultadoMetodo = escalonaMatriz.escalonaMatrizGauss(mat1);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}
}