/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.matriz.IVerificaMatrizInversa;
import br.calculator.matriz.MatrizException;
import br.calculator.matriz.VerificaMatrizInversa;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 6.0
 */
public class VerificaMatrizInversaTeste {

	final IVerificaMatrizInversa verificaInversa = new VerificaMatrizInversa();
	
	@Test
	public void deveVerificarMatrizInversaInt() throws MatrizException {
		final int[][] teste1Mat1 = new int[][] {{0, 0}, {0, 0}};

		final int[][] teste1Mat2 = new int[][] {{0, 0}, {0, 0}};

		final boolean resposta1 = verificaInversa.verificaMatrizInversa(teste1Mat1, teste1Mat2);

		//Teste 1
		Assert.assertTrue(!resposta1);

		final int[][] teste2Mat1 = new int[][] {{3, 0, 2}, {9, 1, 7}, {1, 0, 1}};

		final int[][] teste2Mat2 = new int[][] {{1, 0, -2}, {-2, 1,-3}, {1, 0, 3}};

		final boolean resposta2 = verificaInversa.verificaMatrizInversa(teste2Mat1, teste2Mat2);

		//Teste 2
		Assert.assertTrue(resposta2);
		
		final int[][] teste3Mat1 = new int[][] {{2, 5}, {1, 3}};

		final int[][] teste3Mat2 = new int[][] {{1, 2}, {1, 1}};

		final boolean resposta3 = verificaInversa.verificaMatrizInversa(teste3Mat1, teste3Mat2);

		//Teste 3
		Assert.assertTrue(!resposta3);
	}
	
	@Test
	public void deveVerificarMatrizInversaLong() throws MatrizException {
		final long[][] mat1 = new long[][] {{3, 0, 2}, {9, 1, 7}, {1, 0, 1}};

		final long[][] mat2 = new long[][] {{1, 0, -2}, {-2, 1, -3}, {-1, 0, 3}};
		
		final boolean resposta = verificaInversa.verificaMatrizInversa(mat1, mat2);
		
		Assert.assertTrue(resposta);
	}
	
	@Test
	public void deveVerificarMatrizInversaFloat() throws MatrizException {
		final float[][] mat1 = new float[][] {{3, 0, 2}, {9, 1, 7}, {1, 0, 1}};

		final float[][] mat2 = new float[][] {{1, 0, -2}, {-2, 1, -3}, {-1, 0, 3}};
		
		final boolean resposta = verificaInversa.verificaMatrizInversa(mat1, mat2);
		
		Assert.assertTrue(resposta);
	}
	
	@Test
	public void deveVerificarMatrizInversaDouble() throws MatrizException {
		final double[][] mat1 = new double[][] {{3, 0, 2}, {9, 1, 7}, {1, 0, 1}};

		final double[][] mat2 = new double[][] {{1, 0, -2}, {-2, 1, -3}, {-1, 0, 3}};
		
		final boolean resposta = verificaInversa.verificaMatrizInversa(mat1, mat2);
		
		Assert.assertTrue(resposta);
	}
}