/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.matriz.IMultiplicaMatrizPorEscalar;
import br.calculator.matriz.MatrizException;
import br.calculator.matriz.MultiplicaMatrizPorEscalar;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 2.0
 */
public class MultiplicaMatrizPorEscalarTeste {

	final IMultiplicaMatrizPorEscalar multi = new MultiplicaMatrizPorEscalar();

	@Test
	public void deveMultiplicaMatrizPorEscalarIntInt() throws MatrizException {
		final int[][] matriz1 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int escalar1 = 0;

		final int[][] resultadoReal1 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] resultadoMetodo1 = multi.multiplicaMatrizPorEscalar(escalar1, matriz1);

		//Teste 1
		Assert.assertArrayEquals(resultadoReal1, resultadoMetodo1);

		final int[][] matriz2 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int escalar2 = 1;

		final int[][] resultadoReal2 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] resultadoMetodo2 = multi.multiplicaMatrizPorEscalar(escalar2, matriz2);

		//Teste 2
		Assert.assertArrayEquals(resultadoReal2, resultadoMetodo2);
		
		final int[][] matriz3 = new int[][] {{1, 2, 3}, {4, 5, 6}};

		final int escalar3 = -1;

		final int[][] resultadoReal3 = new int[][] {{-1, -2, -3}, {-4, -5, -6}};

		final int[][] resultadoMetodo3 = multi.multiplicaMatrizPorEscalar(escalar3, matriz3);

		//Teste 3
		Assert.assertArrayEquals(resultadoReal3, resultadoMetodo3);
		
		final int[][] matriz4 = new int[][] {{-1, -2, -3}, {-4, -5, -6}};

		final int escalar4 = 1;

		final int[][] resultadoReal4 = new int[][] {{-1, -2, -3}, {-4, -5, -6}};

		final int[][] resultadoMetodo4 = multi.multiplicaMatrizPorEscalar(escalar4, matriz4);

		//Teste 4
		Assert.assertArrayEquals(resultadoReal4, resultadoMetodo4);
		
		final int[][] matriz5 = new int[][] {{1, 2, 3}, {4, 5, 6}};

		final int escalar5 = 1;

		final int[][] resultadoReal5 = new int[][] {{1, 2, 3}, {4, 5, 6}};

		final int[][] resultadoMetodo5 = multi.multiplicaMatrizPorEscalar(escalar5, matriz5);

		//Teste 5
		Assert.assertArrayEquals(resultadoReal5, resultadoMetodo5);
	}

	@Test
	public void deveMultiplicaMatrizPorEscalarIntLong() throws MatrizException {
		final long[][] matriz = new long[][] {{2, 4, 6}, {8, 10, 12}, {14, 16, 18}};

		final int escalar = 2;

		final long[][] resultadoReal = new long[][] {{4, 8, 12}, {16, 20, 24}, {28, 32, 36}};

		final long[][] resultadoMetodo = multi.multiplicaMatrizPorEscalar(escalar, matriz);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizPorEscalarIntFloat() throws MatrizException {
		final float[][] matriz = new float[][] {{2.2f, 4.4f, 6.6f}, {8.8f, 10.1f, 12.12f}, {14.14f, 16.16f, 18.18f}};

		final int escalar = 2;

		final float[][] resultadoReal = new float[][] {{4.4f, 8.8f, 13.2f}, {17.6f, 20.2f, 24.24f}, {28.28f, 32.32f, 36.36f}};

		final float[][] resultadoMetodo = multi.multiplicaMatrizPorEscalar(escalar, matriz);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizPorEscalarIntDouble() throws MatrizException {
		final double[][] matriz = new double[][] {{2.2, 4.4, 6.6}, {8.8, 10.1, 12.12}, {14.14, 16.16, 18.18}};

		final int escalar = 2;

		final double[][] resultadoReal = new double[][] {{4.4, 8.8, 13.2}, {17.6, 20.2, 24.24}, {28.28, 32.32, 36.36}};

		final double[][] resultadoMetodo = multi.multiplicaMatrizPorEscalar(escalar, matriz);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizPorEscalarLongLong() throws MatrizException {
		final long[][] matriz = new long[][] {{2, 4, 6}, {8, 10, 12}, {14, 16, 18}};

		final long escalar = 2;

		final long[][] resultadoReal = new long[][] {{4, 8, 12}, {16, 20, 24}, {28, 32, 36}};

		final long[][] resultadoMetodo = multi.multiplicaMatrizPorEscalar(escalar, matriz);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizPorEscalarFloatFloat() throws MatrizException {
		final float[][] matriz = new float[][] {{2.2f, 4.4f, 6.6f}, {8.8f, 10.1f, 12.12f}, {14.14f, 16.16f, 18.18f}};

		final float escalar = 2;

		final float[][] resultadoReal = new float[][] {{4.4f, 8.8f, 13.2f}, {17.6f, 20.2f, 24.24f}, {28.28f, 32.32f, 36.36f}};

		final float[][] resultadoMetodo = multi.multiplicaMatrizPorEscalar(escalar, matriz);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveMultiplicaMatrizPorEscalarDoubleDouble() throws MatrizException {
		final double[][] matriz = new double[][] {{2.2, 4.4, 6.6}, {8.8, 10.1, 12.12}, {14.14, 16.16, 18.18}};

		final double escalar = 2;

		final double[][] resultadoReal = new double[][] {{4.4, 8.8, 13.2}, {17.6, 20.2, 24.24}, {28.28, 32.32, 36.36}};

		final double[][] resultadoMetodo = multi.multiplicaMatrizPorEscalar(escalar, matriz);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}
}