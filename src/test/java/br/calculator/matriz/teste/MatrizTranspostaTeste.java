/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.matriz.IMatrizTransposta;
import br.calculator.matriz.MatrizException;
import br.calculator.matriz.MatrizTransposta;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 16.0
 */
public class MatrizTranspostaTeste {
	final IMatrizTransposta matrizTransposta = new MatrizTransposta();
	
	@Test
	public void deveVerificarMatrizTranspostaInt() throws MatrizException {
		final int[][] teste1Mat1 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1ResultadoReal = new int[][] {{0, 0}, {0, 0}, {0, 0}};

		final int[][] teste1ResultadoMetodo = matrizTransposta.matrizTransposta(teste1Mat1);

		//Teste 1
		Assert.assertArrayEquals(teste1ResultadoReal, teste1ResultadoMetodo);

		final int[][] teste2Mat1 = new int[][] {{-1, -2, -3}, {-4, -5, -6}};

		final int[][] teste2ResultadoReal = new int[][] {{-1, -4}, {-2, -5}, {-3, -6}};

		final int[][] teste2ResultadoMetodo = matrizTransposta.matrizTransposta(teste2Mat1);

		//Teste 2
		Assert.assertArrayEquals(teste2ResultadoReal, teste2ResultadoMetodo);
		
		final int[][] teste3Mat1 = new int[][] {{1, 2, 3}, {4, 5, 6}};

		final int[][] teste3ResultadoReal = new int[][] {{1, 4}, {2, 5}, {3, 6}};

		final int[][] teste3ResultadoMetodo = matrizTransposta.matrizTransposta(teste3Mat1);

		//Teste 3
		Assert.assertArrayEquals(teste3ResultadoReal, teste3ResultadoMetodo);
		
		final int[][] teste4Mat1 = new int[][] {{10, 22, 33}, {44, 55, 66}};

		final int[][] teste4ResultadoReal = new int[][] {{10, 44}, {22, 55}, {33, 66}};

		final int[][] teste4ResultadoMetodo = matrizTransposta.matrizTransposta(teste4Mat1);

		//Teste 4
		Assert.assertArrayEquals(teste4ResultadoReal, teste4ResultadoMetodo);		
	}

	@Test
	public void deveVerificarMatrizTranspostaLong() throws MatrizException {
		final long[][] mat1 = new long[][] {{2, 5, 9}, {3, 6, 8}};

		final long[][] resultadoReal = new long[][] {{2, 3}, {5, 6}, {9, 8}};

		final long[][] resultadoMetodo = matrizTransposta.matrizTransposta(mat1);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveVerificarMatrizTranspostaFloat() throws MatrizException {
		final float[][] mat1 = new float[][] {{2, 5, 9}, {3, 6, 8}};

		final float[][] resultadoReal = new float[][] {{2, 3}, {5, 6}, {9, 8}};

		final float[][] resultadoMetodo = matrizTransposta.matrizTransposta(mat1);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveVerificarMatrizTranspostaDouble() throws MatrizException {
		final double[][] mat1 = new double[][] {{2, 5, 9}, {3, 6, 8}};

		final double[][] resultadoReal = new double[][] {{2, 3}, {5, 6}, {9, 8}};

		final double[][] resultadoMetodo = matrizTransposta.matrizTransposta(mat1);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}
}