/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.matriz.ISubtrairMatriz;
import br.calculator.matriz.MatrizException;
import br.calculator.matriz.SubtrairMatriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 2.0
 */
public class SubtrairMatrizTeste {

	final ISubtrairMatriz subtrai = new SubtrairMatriz();

	@Test
	public void deveSubtrairMatrizInt() throws MatrizException {
		final int[][] teste1Mat1 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1Mat2 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1ResultadoReal = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1ResultadoMetodo = subtrai.subtrairMatriz(teste1Mat1, teste1Mat2);

		//Teste 1
		Assert.assertArrayEquals(teste1ResultadoReal, teste1ResultadoMetodo);
		
		final int[][] teste3Mat1 = new int[][] {{-1, 0, -4}, {-2, 0, -3}};

		final int[][] teste3Mat2 = new int[][] {{0, -5, 0}, {0, -6, 0}};

		final int[][] teste3ResultadoReal = new int[][] {{-1, 5, -4}, {-2, 6, -3}};

		final int[][] teste3ResultadoMetodo = subtrai.subtrairMatriz(teste3Mat1, teste3Mat2);

		//Teste 3
		Assert.assertArrayEquals(teste3ResultadoReal, teste3ResultadoMetodo);
		
		final int[][] teste4Mat1 = new int[][] {{1, 0, 4}, {2, 0, 3}};

		final int[][] teste4Mat2 = new int[][] {{0, 5, 0}, {0, 6, 0}};

		final int[][] teste4ResultadoReal = new int[][] {{1, -5, 4}, {2, -6, 3}};

		final int[][] teste4ResultadoMetodo = subtrai.subtrairMatriz(teste4Mat1, teste4Mat2);

		//Teste 4
		Assert.assertArrayEquals(teste4ResultadoReal, teste4ResultadoMetodo);
	}

	@Test
	public void deveSubtrairMatrizLong() throws MatrizException {
		final long[][] mat1 = new long[][] {{2000, 4000, 6000}, {8000, 10000, 12000}, {14000, 16000, 18000}};

		final long[][] mat2 = new long[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final long[][] resultadoReal = new long[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final long[][] resultadoMetodo = subtrai.subtrairMatriz(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveSubtrairMatrizFloat() throws MatrizException {
		final float[][] mat1 = new float[][] {{2000, 4000, 6000}, {8000, 10000, 12000}, {14000, 16000, 18000}};

		final float[][] mat2 = new float[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final float[][] resultadoReal = new float[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final float[][] resultadoMetodo = subtrai.subtrairMatriz(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveSubtrairMatrizDouble() throws MatrizException {
		final double[][] mat1 = new double[][] {{2000, 4000, 6000}, {8000, 10000, 12000}, {14000, 16000, 18000}};

		final double[][] mat2 = new double[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final double[][] resultadoReal = new double[][] {{1000, 2000, 3000}, {4000, 5000, 6000}, {7000, 8000, 9000}};

		final double[][] resultadoMetodo = subtrai.subtrairMatriz(mat1, mat2);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}
}