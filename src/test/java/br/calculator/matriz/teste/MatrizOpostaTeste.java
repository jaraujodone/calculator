/**
 * 
 */
package br.calculator.matriz.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.matriz.IMatrizOposta;
import br.calculator.matriz.MatrizException;
import br.calculator.matriz.MatrizOposta;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 16.0
 */
public class MatrizOpostaTeste {
	
	final IMatrizOposta matrizOposta= new MatrizOposta();
	
	@Test
	public void deveVerificarMatrizOpostaInt() throws MatrizException {
		final int[][] teste1Mat1 = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1ResultadoReal = new int[][] {{0, 0, 0}, {0, 0, 0}};

		final int[][] teste1ResultadoMetodo = matrizOposta.matrizOposta(teste1Mat1);

		//Teste 1
		Assert.assertArrayEquals(teste1ResultadoReal, teste1ResultadoMetodo);

		final int[][] teste2Mat1 = new int[][] {{-1, -2, -3}, {-4, -5, -6}};

		final int[][] teste2ResultadoReal = new int[][] {{1, 2, 3}, {4, 5, 6}};

		final int[][] teste2ResultadoMetodo = matrizOposta.matrizOposta(teste2Mat1);

		//Teste 2
		Assert.assertArrayEquals(teste2ResultadoReal, teste2ResultadoMetodo);
		
		final int[][] teste3Mat1 = new int[][] {{1, 2, 3}, {4, 5, 6}};

		final int[][] teste3ResultadoReal = new int[][] {{-1, -2, -3}, {-4, -5, -6}};

		final int[][] teste3ResultadoMetodo = matrizOposta.matrizOposta(teste3Mat1);

		//Teste 3
		Assert.assertArrayEquals(teste3ResultadoReal, teste3ResultadoMetodo);
		
		final int[][] teste4Mat1 = new int[][] {{10, 22, 33}, {44, 55, 66}};

		final int[][] teste4ResultadoReal = new int[][] {{-10, -22, -33}, {-44, -55, -66}};

		final int[][] teste4ResultadoMetodo = matrizOposta.matrizOposta(teste4Mat1);

		//Teste 4
		Assert.assertArrayEquals(teste4ResultadoReal, teste4ResultadoMetodo);
	}

	@Test
	public void deveVerificarMatrizOpostaLong() throws MatrizException {
		final long[][] mat1 = new long[][] {{2, 5, 9}, {3, 6, -8}};

		final long[][] resultadoReal = new long[][] {{-2, -5, -9}, {-3, -6, 8}};

		final long[][] resultadoMetodo = matrizOposta.matrizOposta(mat1);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveVerificarMatrizOpostaFloat() throws MatrizException {
		final float[][] mat1 = new float[][] {{2, 5, 9}, {3, 6, -8}};

		final float[][] resultadoReal = new float[][] {{-2, -5, -9}, {-3, -6, 8}};

		final float[][] resultadoMetodo = matrizOposta.matrizOposta(mat1);
		
		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}

	@Test
	public void deveVerificarMatrizOpostaDouble() throws MatrizException {
		final double[][] mat1 = new double[][] {{2, 5, 9}, {3, 6, -8}};

		final double[][] resultadoReal = new double[][] {{-2, -5, -9}, {-3, -6, 8}};

		final double[][] resultadoMetodo = matrizOposta.matrizOposta(mat1);

		Assert.assertArrayEquals(resultadoReal, resultadoMetodo);
	}
}