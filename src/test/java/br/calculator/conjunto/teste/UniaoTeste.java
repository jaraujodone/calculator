/**
 * 
 */
package br.calculator.conjunto.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.conjunto.Uniao;

/**
 * @author Jesse A. Done
 */
public class UniaoTeste {

	final Uniao uniao = new Uniao();
	
	@Test
	public void deveObterUniao() {
		final int[] teste1Vetor1 = new int[] {};
		final int[] teste1Vetor2 = new int[] {};
		final Integer[] teste1Resultado = new Integer[] {};
		
		final Integer[] teste1Uni = uniao.uniao(teste1Vetor1, teste1Vetor2);
		
		//Teste 1
		Assert.assertArrayEquals(teste1Uni, teste1Resultado);
		
		final int[] teste2Vetor1 = new int[] {0};
		final int[] teste2Vetor2 = new int[] {0};
		final Integer[] teste2Resultado = new Integer[] {0};
		
		final Integer[] teste2Uni = uniao.uniao(teste2Vetor1, teste2Vetor2);
		
		//Teste 2
		Assert.assertArrayEquals(teste2Uni, teste2Resultado);
		
		final int[] teste3Vetor1 = new int[] {-1};
		final int[] teste3Vetor2 = new int[] {-2};
		final Integer[] teste3Resultado = new Integer[] {-2, -1};
		
		final Integer[] teste3Uni = uniao.uniao(teste3Vetor1, teste3Vetor2);
		
		//Teste 3
		Assert.assertArrayEquals(teste3Uni, teste3Resultado);
		
		final int[] teste4Vetor1 = new int[] {1, 2, 3};
		final int[] teste4Vetor2 = new int[] {4, 5, 6};
		final Integer[] teste4Resultado = new Integer[] {1, 2, 3, 4, 5, 6};
		
		final Integer[] teste4Uni = uniao.uniao(teste4Vetor1, teste4Vetor2);
		
		//Teste 4
		Assert.assertArrayEquals(teste4Uni, teste4Resultado);
		
		final int[] teste5Vetor1 = new int[] {1, 2, 3};
		final int[] teste5Vetor2 = new int[] {1, 5, 6};
		final Integer[] teste5Resultado = new Integer[] {1, 2, 3, 5, 6};
		
		final Integer[] teste5Uni = uniao.uniao(teste5Vetor1, teste5Vetor2);
		
		//Teste 5
		Assert.assertArrayEquals(teste5Uni, teste5Resultado);
	}
}