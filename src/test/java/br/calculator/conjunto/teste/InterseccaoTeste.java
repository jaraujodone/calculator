/**
 * 
 */
package br.calculator.conjunto.teste;

import org.junit.Assert;
import org.junit.Test;

import br.calculator.conjunto.Interseccao;

/**
 * @author Jesse A. Done
 */
public class InterseccaoTeste {

	final Interseccao interseccao = new Interseccao();
	
	@Test
	public void deveObterInterseccao() {
		final int[] teste1Vetor1 = new int[] {};
		final int[] teste1Vetor2 = new int[] {};
		final Integer[] teste1Resultado = new Integer[] {};
		
		final Integer[] teste1Int = interseccao.interseccao(teste1Vetor1, teste1Vetor2);
		
		//Teste 1
		Assert.assertArrayEquals(teste1Int, teste1Resultado);
		
		final int[] teste2Vetor1 = new int[] {0};
		final int[] teste2Vetor2 = new int[] {0};
		final Integer[] teste2Resultado = new Integer[] {0};
		
		final Integer[] teste2Int = interseccao.interseccao(teste2Vetor1, teste2Vetor2);
		
		//Teste 2
		Assert.assertArrayEquals(teste2Int, teste2Resultado);
		
		final int[] teste3Vetor1 = new int[] {-1};
		final int[] teste3Vetor2 = new int[] {-2};
		final Integer[] teste3Resultado = new Integer[] {};
		
		final Integer[] teste3Int = interseccao.interseccao(teste3Vetor1, teste3Vetor2);
		
		//Teste 3
		Assert.assertArrayEquals(teste3Int, teste3Resultado);
		
		final int[] teste4Vetor1 = new int[] {-1};
		final int[] teste4Vetor2 = new int[] {-1};
		final Integer[] teste4Resultado = new Integer[] {-1};
		
		final Integer[] teste4Int = interseccao.interseccao(teste4Vetor1, teste4Vetor2);
		
		//Teste 4
		Assert.assertArrayEquals(teste4Int, teste4Resultado);
		
		final int[] teste5Vetor1 = new int[] {1, 2, 3};
		final int[] teste5Vetor2 = new int[] {4, 5, 6};
		final Integer[] teste5Resultado = new Integer[] {};
		
		final Integer[] teste5Int = interseccao.interseccao(teste5Vetor1, teste5Vetor2);
		
		//Teste 5
		Assert.assertArrayEquals(teste5Int, teste5Resultado);
		
		final int[] teste6Vetor1 = new int[] {1, 2, 3};
		final int[] teste6Vetor2 = new int[] {1, 5, 6};
		final Integer[] teste6Resultado = new Integer[] {1};
		
		final Integer[] teste6Int = interseccao.interseccao(teste6Vetor1, teste6Vetor2);
		
		//Teste 6
		Assert.assertArrayEquals(teste6Int, teste6Resultado);
	}
}