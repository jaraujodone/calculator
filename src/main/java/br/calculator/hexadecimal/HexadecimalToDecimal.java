package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class HexadecimalToDecimal implements IHexadecimalToDecimal {

	@Override
	public int hexadecimalToDecimal(final String hexadecimal) {
		return Integer.parseInt(hexadecimal, 16);
	}
}