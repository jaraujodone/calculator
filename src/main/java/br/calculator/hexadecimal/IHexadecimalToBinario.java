package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IHexadecimalToBinario {
	
	/**
	 * Metodo responsavel por converter um hexadecimal em um binario
	 * 
	 * @param hexadecimal
	 * @return
	 */
	public String hexadecimalToBinario(final String hexadecimal);
}