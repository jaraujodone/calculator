package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IHexadecimalToDecimal {
	
	/**
	 * Metodo responsavel por converter um hexadecimal em um decimal
	 * 
	 * @param hexadecimal
	 * @return
	 */
	public int hexadecimalToDecimal(final String hexadecimal);
}