package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class HexadecimalToOctal implements IHexadecimalToOctal {

	@Override
	public String hexadecimalToOctal(final String hexadecimal) {
		int numero = Integer.parseInt(hexadecimal, 16);
		return Integer.toOctalString(numero);
	}
}