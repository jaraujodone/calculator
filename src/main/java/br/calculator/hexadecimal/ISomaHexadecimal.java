package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface ISomaHexadecimal {
	
	/**
	 * Metodo responsavel por somar dois numeros decimal
	 *  e tranformar o resultado em hexadecimal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String somaHexadecimal(final int numero1, final int numero2);

	/**
	 * Metodo responsavel por somar dois numeros hexadecimal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String somaHexadecimal(final String numero1, final String numero2);
}