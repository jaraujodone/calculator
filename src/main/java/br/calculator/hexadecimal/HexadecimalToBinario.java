package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class HexadecimalToBinario implements IHexadecimalToBinario {

	@Override
	public String hexadecimalToBinario(final String hexadecimal) {
		int numero = Integer.parseInt(hexadecimal, 16);
		return Integer.toBinaryString(numero);
	}
}