package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 7.0
 */
public class SubtrairHexadecimal implements ISubtrairHexadecimal {

	@Override
	public String subtraiHexadecimal(int numero1, int numero2) {
		return Integer.toHexString(numero1 - numero2);
	}

	@Override
	public String subtraiHexadecimal(String numero1, String numero2) {
		return Integer.toHexString((Integer.parseInt(numero1, 16) - Integer.parseInt(numero2, 16)));
	}
}