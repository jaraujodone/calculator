package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IMultiplicarHexadecimal {
	
	/**
	 * Metodo responsavel por multiplicar dois numeros decimal
	 *  e tranformar o resultado em hexadecimal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String multiplicaHexadecimal(final int numero1, final int numero2);

	/**
	 * Metodo responsavel por multiplicar dois numeros hexadecimal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String multiplicaHexadecimal(final String numero1, final String numero2);
}