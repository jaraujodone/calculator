package br.calculator.hexadecimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IHexadecimalToOctal {
	
	/**
	 * Metodo responsavel por converter um hexadecimal em um octal
	 * 
	 * @param hexadecimal
	 * @return
	 */
	public String hexadecimalToOctal(final String hexadecimal);
}