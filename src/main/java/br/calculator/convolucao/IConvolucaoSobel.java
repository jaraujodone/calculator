/**
 * 
 */
package br.calculator.convolucao;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 13.0
 */
public interface IConvolucaoSobel {

	/**
	 * Metodo responsavel por fazer a convolucao pelo filtro de sobel horizontal
	 * sempreo execultando nos pontos da matriz em que se tem
	 * todos os vizinhos para a execucao do calculo
	 * 
	 * @param matriz
	 * @return
	 * @throws ConvolucaoException
	 */
	public int[][] convolucaoSobelHorizontal(int[][] matriz) throws ConvolucaoException;
	
	/**
	 * Metodo responsavel por fazer a convolucao pelo filtro de sobel vertical
	 * sempreo execultando nos pontos da matriz em que se tem
	 * todos os vizinhos para a execucao do calculo
	 * 
	 * @param matriz
	 * @return
	 * @throws ConvolucaoException
	 */
	public int[][] convolucaoSobelVertical(int[][] matriz) throws ConvolucaoException;
}