/**
 * 
 */
package br.calculator.convolucao;

/**
 * @author Jesse A. Done
 */
public class ConvolucaoException extends Exception {
	private static final long serialVersionUID = 2061684326402592270L;

	/**
	 * @param paramString
	 */
	public ConvolucaoException(final String paramString) {
		super(paramString);
	}
	
	/**
	 * @param paramString
	 * @param paramThrowable
	 */
	public ConvolucaoException(final String paramString, final Throwable paramThrowable) {
		super(paramString, paramThrowable);
	}
}