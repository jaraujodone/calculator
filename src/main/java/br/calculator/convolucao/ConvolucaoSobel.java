/**
 * 
 */
package br.calculator.convolucao;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 13.0
 */
public class ConvolucaoSobel implements IConvolucaoSobel {
	
	private IConvolucao3x3 convolucao3x3 = new Convolucao3x3();
	
	private static final int[][] SOBEL_HORIZONTAL = 
			new int[][] {{-1, 2, -1}, {0, 0, 0}, {1, 2, 1}};
	
	private static final int[][] SOBEL_VERTICAL = 
			new int[][] {{-1, 0, 1}, {2, 0, 2}, {-1, 0, 1}};
	
	@Override
	public int[][] convolucaoSobelHorizontal(int[][] matriz) throws ConvolucaoException {
		final int[][] matrizResultado = 
				convolucao3x3.convolucaoFiltro3x3(matriz, SOBEL_HORIZONTAL);
		
		return matrizResultado;
	}
	
	@Override
	public int[][] convolucaoSobelVertical(int[][] matriz) throws ConvolucaoException {
		final int[][] matrizResultado = 
				convolucao3x3.convolucaoFiltro3x3(matriz, SOBEL_VERTICAL);
		
		return matrizResultado;
	}
}