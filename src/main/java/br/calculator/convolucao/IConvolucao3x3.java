/**
 * 
 */
package br.calculator.convolucao;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 13.0
 */
public interface IConvolucao3x3 {

	/**
	 * Metodo responsavel por fazer a convolucao de filtro 3 x 3
	 * sempreo execultando nos pontos da matriz em que se tem
	 * todos os vizinhos para a execucao do calculo
	 * 
	 * @param matriz
	 * @param filtro
	 * @return
	 * @throws ConvolucaoException
	 */
	public int[][] convolucaoFiltro3x3(int[][] matriz, int[][] filtro) throws ConvolucaoException;
}