/**
 * 
 */
package br.calculator.convolucao;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 13.0
 */
public class Convolucao3x3 implements IConvolucao3x3 {

	@Override
	public int[][] convolucaoFiltro3x3(int[][] matriz, int[][] filtro) throws ConvolucaoException {
		//valida os parametros necessario para a execucao correta do metodo
		validarParametro(matriz, filtro);
		
		int[][] matrizResultado = obterValoresMatriz(matriz);
		
		int somador = 0;
		
		for(int iMat = 1; iMat < matriz.length - 1; iMat++) {
			for(int jMat = 1; jMat < matriz[0].length -1; jMat++) {
				for(int i = -1; i < 2; i++) {
					for(int j = -1; j < 2; j++) {
						somador += filtro[(i + 1)][(j + 1)] * matriz[iMat - i][jMat - j];
					}
				}
				
				matrizResultado[iMat][jMat] = somador;
				somador = 0;
			}
		}
		
		return matrizResultado;
	}
	
	/**
	 * @param matriz
	 * @return
	 */
	private int[][] obterValoresMatriz(final int[][] matriz) {
		int[][] matrizResultado = new int[matriz.length][matriz[0].length];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				matrizResultado[i][j] = matriz[i][j];
			}
		}
		
		return matrizResultado;
	}
	
	/**
	 * @param matriz
	 * @param filtro
	 * @throws ConvolucaoException
	 */
	private void validarParametro(int[][] matriz, int[][] filtro) throws ConvolucaoException {
		if(matriz == null || filtro == null) {
			throw new ConvolucaoException("Nem a matriz da imagem nem o filtro podem ser nulos");
		}
		
		if(filtro.length != 3 && filtro[0].length != 3) {
			throw new ConvolucaoException("Filtro tem dimensoes diferente de 3 x 3");
		}
	}
}