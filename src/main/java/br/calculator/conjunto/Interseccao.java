/**
 * 
 */
package br.calculator.conjunto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Jesse A. Done
 */
public class Interseccao implements IInterseccao {

	@Override
	public Integer[] interseccao(final int[] vetor1, final int[] vetor2) {
		Set<Integer> uni = new HashSet<Integer>();

		for (int i = 0; i < vetor1.length; i++) {
			for (int j = 0; j < vetor2.length; j++) {
				if (vetor1[i] == vetor2[j]) {
					uni.add(vetor1[i]);
				}
			}
		}

		final Integer[] uniao = uni.toArray(new Integer[0]);

		Arrays.sort(uniao);

		return uniao;
	}

	@Override
	public Long[] interseccao(final long[] vetor1, final long[] vetor2) {
		Set<Long> uni = new HashSet<Long>();

		for (int i = 0; i < vetor1.length; i++) {
			for (int j = 0; j < vetor2.length; j++) {
				if (vetor1[i] == vetor2[j]) {
					uni.add(vetor1[i]);
				}
			}
		}

		final Long[] uniao = uni.toArray(new Long[0]);

		Arrays.sort(uniao);

		return uniao;
	}

	@Override
	public Float[] interseccao(final float[] vetor1, final float[] vetor2) {
		Set<Float> uni = new HashSet<Float>();

		for (int i = 0; i < vetor1.length; i++) {
			for (int j = 0; j < vetor2.length; j++) {
				if (vetor1[i] == vetor2[j]) {
					uni.add(vetor1[i]);
				}
			}
		}

		final Float[] uniao = uni.toArray(new Float[0]);

		Arrays.sort(uniao);

		return uniao;
	}

	@Override
	public Double[] interseccao(final double[] vetor1, final double[] vetor2) {
		Set<Double> uni = new HashSet<Double>();

		for (int i = 0; i < vetor1.length; i++) {
			for (int j = 0; j < vetor2.length; j++) {
				if (vetor1[i] == vetor2[j]) {
					uni.add(vetor1[i]);
				}
			}
		}

		final Double[] uniao = uni.toArray(new Double[0]);

		Arrays.sort(uniao);

		return uniao;
	}
}