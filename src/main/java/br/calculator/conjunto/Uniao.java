/**
 * 
 */
package br.calculator.conjunto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Jesse A. Done
 */
public class Uniao implements IUniao {

	@Override
	public Integer[] uniao(final int[] vetor1, final int[] vetor2) {
		Set<Integer> uni = new HashSet<Integer>();

		for (int i = 0; i < vetor1.length; i++) {
			uni.add(vetor1[i]);
		}

		for (int i = 0; i < vetor2.length; i++) {
			uni.add(vetor2[i]);
		}

		final Integer[] uniao = uni.toArray(new Integer[0]);

		Arrays.sort(uniao);

		return uniao;
	}

	@Override
	public Long[] uniao(final long[] vetor1, final long[] vetor2) {
		Set<Long> uni = new HashSet<Long>();

		for (int i = 0; i < vetor1.length; i++) {
			uni.add(vetor1[i]);
		}

		for (int i = 0; i < vetor2.length; i++) {
			uni.add(vetor2[i]);
		}

		final Long[] uniao = uni.toArray(new Long[0]);

		Arrays.sort(uniao);

		return uniao;
	}

	@Override
	public Float[] uniao(final float[] vetor1, final float[] vetor2) {
		Set<Float> uni = new HashSet<Float>();

		for (int i = 0; i < vetor1.length; i++) {
			uni.add(vetor1[i]);
		}

		for (int i = 0; i < vetor2.length; i++) {
			uni.add(vetor2[i]);
		}

		final Float[] uniao = uni.toArray(new Float[0]);

		Arrays.sort(uniao);

		return uniao;
	}

	@Override
	public Double[] uniao(final double[] vetor1, final double[] vetor2) {
		Set<Double> uni = new HashSet<Double>();

		for (int i = 0; i < vetor1.length; i++) {
			uni.add(vetor1[i]);
		}

		for (int i = 0; i < vetor2.length; i++) {
			uni.add(vetor2[i]);
		}

		final Double[] uniao = uni.toArray(new Double[0]);

		Arrays.sort(uniao);

		return uniao;
	}
}