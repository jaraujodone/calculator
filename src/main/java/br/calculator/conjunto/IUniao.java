/**
 * 
 */
package br.calculator.conjunto;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IUniao {

	/**
	 * Metodo responsavel por fazer a uniao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Integer[] uniao(final int[] vetor1, final int[] vetor2);
	
	/**
	 * Metodo responsavel por fazer a uniao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Long[] uniao(final long[] vetor1, final long[] vetor2);
	
	/**
	 * Metodo responsavel por fazer a uniao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Float[] uniao(final float[] vetor1, final float[] vetor2);
	
	/**
	 * Metodo responsavel por fazer a uniao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Double[] uniao(final double[] vetor1, final double[] vetor2);
}