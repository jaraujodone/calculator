/**
 * 
 */
package br.calculator.conjunto;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IInterseccao {

	/**
	 * Metodo responsavel por fazer a interseccao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Integer[] interseccao(final int[] vetor1, final int[] vetor2);
	
	/**
	 * Metodo responsavel por fazer a interseccao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Long[] interseccao(final long[] vetor1, final long[] vetor2);
	
	/**
	 * Metodo responsavel por fazer a interseccao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Float[] interseccao(final float[] vetor1, final float[] vetor2);
	
	/**
	 * Metodo responsavel por fazer a interseccao de dois vetores
	 * 
	 * @param vetor1
	 * @param vetor2
	 * @return
	 */
	public Double[] interseccao(final double[] vetor1, final double[] vetor2);
}