package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IDividirOctal {
	
	/**
	 * Metodo responsavel por dividir dois numeros decimal
	 *  e tranformar o resultado em octal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String dividiOctal(final int numero1, final int numero2);

	/**
	 * Metodo responsavel por dividir dois numeros octal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String dividiOctal(final String numero1, final String numero2);
}