package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IOctalToBinario {
	
	/**
	 * Metodo responsavel por converter um octal em um binario
	 * 
	 * @param octal
	 * @return
	 */
	public String octalToBinario(final String octal);
}