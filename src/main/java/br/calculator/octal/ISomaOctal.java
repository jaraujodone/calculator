package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface ISomaOctal {
	
	/**
	 * Metodo responsavel por somar dois numeros decimal
	 *  e tranformar o resultado em octal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String somaOctal(final int numero1, final int numero2);

	/**
	 * Metodo responsavel por somar dois numeros octal
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String somaOctal(final String numero1, final String numero2);
}