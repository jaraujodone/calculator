package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IOctalToHexadecimal {
	
	/**
	 * Metodo responsavel por converter um octal em um hexadecimal
	 * 
	 * @param octal
	 * @return
	 */
	public String octalToHexadecimal(final String octal);
}