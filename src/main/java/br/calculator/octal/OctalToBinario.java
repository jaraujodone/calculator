package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class OctalToBinario implements IOctalToBinario {

	@Override
	public String octalToBinario(final String octal) {
		int numero = Integer.parseInt(octal, 8);
		return Integer.toBinaryString(numero);
	}
}