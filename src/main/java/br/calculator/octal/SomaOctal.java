package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 9.0
 */
public class SomaOctal implements ISomaOctal {

	@Override
	public String somaOctal(int numero1, int numero2) {
		return Integer.toOctalString(numero1 + numero2);
	}

	@Override
	public String somaOctal(String numero1, String numero2) {
		return Integer.toOctalString((Integer.parseInt(numero1, 8) + Integer.parseInt(numero2, 8)));
	}
}