package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class OctalToDecimal implements IOctalToDecimal {

	@Override
	public int octalToDecimal(final String octal) {
		return Integer.parseInt(octal, 8);
	}
}