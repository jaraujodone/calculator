package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IOctalToDecimal {
	
	/**
	 * Metodo responsavel por converter um octal em um decimal
	 * 
	 * @param octal
	 * @return
	 */
	public int octalToDecimal(final String octal);
}