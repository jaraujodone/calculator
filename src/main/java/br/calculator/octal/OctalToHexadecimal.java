package br.calculator.octal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class OctalToHexadecimal implements IOctalToHexadecimal {

	@Override
	public String octalToHexadecimal(final String octal) {
		int numero = Integer.parseInt(octal, 8);
		return Integer.toHexString(numero);
	}
}