/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class BinarioToDecimal implements IBinarioToDecimal {

	@Override
	public int binarioToDecimal(final String binario) {
		return Integer.parseInt(binario, 2);
	}
}