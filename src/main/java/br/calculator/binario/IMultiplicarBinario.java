/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IMultiplicarBinario {
	
	/**
	 * Metodo responsavel por multiplicar dois numeros decimal
	 *  e tranformar o resultado em binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String multiplicarBinario(final int numero1, final int numero2);

	/**
	 * Metodo responsavel por multiplicar dois numeros binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String multiplicarBinario(final String numero1, final String numero2);
}