/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IDividirBinario {
	
	/**
	 * Metodo responsavel por dividir dois numeros decimal
	 *  e tranformar o resultado em binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String dividirBinario(final int numero1, final int numero2);

	/**
	 * Metodo responsavel por dividir dois numeros binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String dividirBinario(final String numero1, final String numero2);
}