/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 3.0
 */
public class MultiplicarBinario implements IMultiplicarBinario {

	@Override
	public String multiplicarBinario(int numero1, int numero2) {
		return Integer.toBinaryString(numero1 * numero2);
	}

	@Override
	public String multiplicarBinario(String numero1, String numero2) {
		return Integer.toBinaryString((Integer.parseInt(numero1, 2) * Integer.parseInt(numero2, 2)));
	}
}