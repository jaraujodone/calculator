package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class BinarioToOctal implements IBinarioToOctal {

	@Override
	public String binarioToOctal(String binario) {
		int numero = Integer.parseInt(binario, 2);
		return Integer.toOctalString(numero);
	}
}