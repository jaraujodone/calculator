package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IBinarioToDecimal {
	
	/**
	 * Metodo responsavel por converter um binario em um decimal
	 * 
	 * @param binario
	 * @return
	 */
	public int binarioToDecimal(final String binario);
}