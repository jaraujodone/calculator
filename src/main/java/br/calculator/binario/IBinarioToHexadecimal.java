package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IBinarioToHexadecimal {
	
	/**
	 * Metodo responsavel por converter um binario em um hexadecimal
	 * 
	 * @param binario
	 * @return
	 */
	public String binarioToHexadecimal(final String binario);
}