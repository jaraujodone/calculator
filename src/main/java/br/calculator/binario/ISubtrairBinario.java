/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface ISubtrairBinario {
	
	/**
	 * Metodo responsavel por subtrair dois numeros decimal
	 *  e tranformar o resultado em binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String subtrairBinario(final int numero1, final int numero2);

	/**
	 * Metodo responsavel por subtrair dois numeros binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String subtrairBinario(final String numero1, final String numero2);
}