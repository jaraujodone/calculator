package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IBinarioToOctal {
	
	/**
	 * Metodo responsavel por converter um binario em um octal
	 * 
	 * @param binario
	 * @return
	 */
	public String binarioToOctal(final String binario);
}