/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 3.0
 */
public class SubtrairBinario implements ISubtrairBinario {
	
	@Override
	public String subtrairBinario(final int numero1, final int numero2) {
		return Integer.toBinaryString(numero1 - numero2);
	}

	@Override
	public String subtrairBinario(final String numero1, final String numero2) {
		return Integer.toBinaryString((Integer.parseInt(numero1, 2) - Integer.parseInt(numero2, 2)));
	}
}
