/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 3.0
 */
public class SomaBinario implements ISomaBinario {

	@Override
	public String somaBinario(final int numero1, final int numero2) {
		return Integer.toBinaryString(numero1 + numero2);
	}
	
	@Override
	public String somaBinario(final String numero1, final String numero2) {
		return Integer.toBinaryString((Integer.parseInt(numero1, 2) + Integer.parseInt(numero2, 2)));
	}
}