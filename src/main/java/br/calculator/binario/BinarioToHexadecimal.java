package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class BinarioToHexadecimal implements IBinarioToHexadecimal {

	@Override
	public String binarioToHexadecimal(final String binario) {
		int numero = Integer.parseInt(binario, 2);
		return Integer.toHexString(numero);
	}
}