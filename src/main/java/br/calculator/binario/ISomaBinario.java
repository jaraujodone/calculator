/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface ISomaBinario {
	
	/**
	 * Metodo responsavel por somar dois numeros decimal
	 *  e tranformar o resultado em binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String somaBinario(final int numero1, final int numero2);
	
	/**
	 * Metodo responsavel por somar dois numeros binario
	 * 
	 * @param numero1
	 * @param numero2
	 * @return
	 */
	public String somaBinario(final String numero1, final String numero2);
}