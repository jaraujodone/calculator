/**
 * 
 */
package br.calculator.binario;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 3.0
 */
public class DividirBinario implements IDividirBinario {

	@Override
	public String dividirBinario(int numero1, int numero2) {
		return Integer.toBinaryString(numero1 / numero2);
	}

	@Override
	public String dividirBinario(String numero1, String numero2) {
		return Integer.toBinaryString((Integer.parseInt(numero1, 2) / Integer.parseInt(numero2, 2)));
	}
}