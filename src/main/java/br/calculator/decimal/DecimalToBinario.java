package br.calculator.decimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class DecimalToBinario implements IDecimalToBinario {

	@Override
	public String decimalToBinario(final int decimal) {
		return Integer.toBinaryString(decimal);
	}
}