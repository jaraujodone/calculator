package br.calculator.decimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class DecimalToHexadecimal implements IDecimalToHexadecimal {

	@Override
	public String decimalToHexadecimal(final int decimal) {
		return Integer.toHexString(decimal);
	}
}