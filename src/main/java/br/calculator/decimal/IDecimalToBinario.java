package br.calculator.decimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IDecimalToBinario {
	
	/**
	 * Metodo responsavel por converter um decimal em um binario
	 * 
	 * @param decimal
	 * @return
	 */
	public String decimalToBinario(final int decimal);
}