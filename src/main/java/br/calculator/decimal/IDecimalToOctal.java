package br.calculator.decimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IDecimalToOctal {
	
	/**
	 * Metodo responsavel por converter um decimal em um octal
	 * 
	 * @param decimal
	 * @return
	 */
	public String decimalToOctal(final int decimal);
}