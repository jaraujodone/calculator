package br.calculator.decimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 11.0
 */
public class DecimalToOctal implements IDecimalToOctal {

	@Override
	public String decimalToOctal(final int decimal) {
		return Integer.toOctalString(decimal);
	}
}