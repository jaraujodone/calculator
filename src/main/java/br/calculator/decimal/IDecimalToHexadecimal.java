package br.calculator.decimal;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IDecimalToHexadecimal {
	
	/**
	 * Metodo responsavel por converter um decimal em um hexadecimal
	 * 
	 * @param decimal
	 * @return
	 */
	public String decimalToHexadecimal(final int decimal);
}