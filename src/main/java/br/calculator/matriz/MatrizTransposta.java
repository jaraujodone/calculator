/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 15.0
 */
public class MatrizTransposta extends AbstractMatriz implements IMatrizTransposta {

	@Override
	public int[][] matrizTransposta(final int[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		int[][] matrizTransposta = new int[matriz[0].length][matriz.length];

		for (int coluna = 0; coluna < matriz[0].length; coluna++) {
			for (int linha = 0; linha < matriz.length; linha++) {
				matrizTransposta[coluna][linha] = matriz[linha][coluna];
			}
		}

		return matrizTransposta;
	}

	@Override
	public long[][] matrizTransposta(final long[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		long[][] matrizTransposta = new long[matriz[0].length][matriz.length];

		for (int coluna = 0; coluna < matriz[0].length; coluna++) {
			for (int linha = 0; linha < matriz.length; linha++) {
				matrizTransposta[coluna][linha] = matriz[linha][coluna];
			}
		}

		return matrizTransposta;
	}

	@Override
	public float[][] matrizTransposta(final float[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		float[][] matrizTransposta = new float[matriz[0].length][matriz.length];

		for (int coluna = 0; coluna < matriz[0].length; coluna++) {
			for (int linha = 0; linha < matriz.length; linha++) {
				matrizTransposta[coluna][linha] = matriz[linha][coluna];
			}
		}

		return matrizTransposta;
	}

	@Override
	public double[][] matrizTransposta(final double[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		double[][] matrizTransposta = new double[matriz[0].length][matriz.length];

		for (int coluna = 0; coluna < matriz[0].length; coluna++) {
			for (int linha = 0; linha < matriz.length; linha++) {
				matrizTransposta[coluna][linha] = matriz[linha][coluna];
			}
		}

		return matrizTransposta;
	}
}