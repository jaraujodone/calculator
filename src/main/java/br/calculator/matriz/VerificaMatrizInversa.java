/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 5.0
 */
public class VerificaMatrizInversa implements IVerificaMatrizInversa {

	final IMultiplicacaoDeMatrizes multMatriz = new MultiplicacaoDeMatrizes();

	@Override
	public boolean verificaMatrizInversa(int[][] mat1, int[][] mat2) throws MatrizException {

		final int[][] matriz = multMatriz.multiplicaMatrizes(mat1, mat2);

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				if (i == j) {
					if (matriz[i][j] != 1) {
						return false;
					}
				} else {
					if (matriz[i][j] != 0) {
						return false;
					}
				}
			}
		}

		return true;
	}

	@Override
	public boolean verificaMatrizInversa(long[][] mat1, long[][] mat2) throws MatrizException {
		final long[][] matriz = multMatriz.multiplicaMatrizes(mat1, mat2);

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				if (i == j) {
					if (matriz[i][j] != 1) {
						return false;
					}
				} else {
					if (matriz[i][j] != 0) {
						return false;
					}
				}
			}
		}

		return true;
	}

	@Override
	public boolean verificaMatrizInversa(float[][] mat1, float[][] mat2) throws MatrizException {
		final float[][] matriz = multMatriz.multiplicaMatrizes(mat1, mat2);

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				if (i == j) {
					if (matriz[i][j] != 1) {
						return false;
					}
				} else {
					if (matriz[i][j] != 0) {
						return false;
					}
				}
			}
		}

		return true;
	}

	@Override
	public boolean verificaMatrizInversa(double[][] mat1, double[][] mat2) throws MatrizException {
		final double[][] matriz = multMatriz.multiplicaMatrizes(mat1, mat2);

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				if (i == j) {
					if (matriz[i][j] != 1) {
						return false;
					}
				} else {
					if (matriz[i][j] != 0) {
						return false;
					}
				}
			}
		}

		return true;
	}
}