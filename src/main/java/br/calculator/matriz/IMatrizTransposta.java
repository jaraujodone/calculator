/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IMatrizTransposta {
	
	/**
	 * Metodo responsavel por calcular a matriz transposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public int[][] matrizTransposta(final int[][] matriz) throws MatrizException;

	/**
	 * Metodo responsavel por calcular a matriz transposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public long[][] matrizTransposta(final long[][] matriz) throws MatrizException;

	/**
	 * Metodo responsavel por calcular a matriz transposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public float[][] matrizTransposta(final float[][] matriz) throws MatrizException;

	/**
	 * Metodo responsavel por calcular a matriz transposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public double[][] matrizTransposta(final double[][] matriz) throws MatrizException;
}