/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IEscalonaMatrizGauss {

	/**
	 * Metodo responsavel por escalonaar a matriz pelo metodo de gauss
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException
	 */
	public float[][] escalonaMatrizGauss(final int[][] matriz) throws MatrizException;
}