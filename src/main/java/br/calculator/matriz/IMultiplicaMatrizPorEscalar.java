/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IMultiplicaMatrizPorEscalar {
	
	/**
	 * Metodo responsavel por multiplicar matriz por um escalar (numero)
	 * 
	 * @param escalar
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public int[][] multiplicaMatrizPorEscalar(int escalar, int[][] matriz) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar matriz por um escalar (numero)
	 * 
	 * @param escalar
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public long[][] multiplicaMatrizPorEscalar(long escalar, long[][] matriz) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar matriz por um escalar (numero)
	 * 
	 * @param escalar
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public long[][] multiplicaMatrizPorEscalar(int escalar, long[][] matriz) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar matriz por um escalar (numero)
	 * 
	 * @param escalar
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public float[][] multiplicaMatrizPorEscalar(float escalar, float[][] matriz) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar matriz por um escalar (numero)
	 * 
	 * @param escalar
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public double[][] multiplicaMatrizPorEscalar(double escalar, double[][] matriz) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar matriz por um escalar (numero)
	 * 
	 * @param escalar
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public float[][] multiplicaMatrizPorEscalar(int escalar, float[][] matriz) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar matriz por um escalar (numero)
	 * 
	 * @param escalar
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public double[][] multiplicaMatrizPorEscalar(int escalar, double[][] matriz) throws MatrizException;
}