/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IMultiplicacaoDeMatrizes {
	
	/**
	 * Metodo responsavel por multiplicar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public int[][] multiplicaMatrizes(int[][] mat1, int[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public long[][] multiplicaMatrizes(long[][] mat1, long[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public float[][] multiplicaMatrizes(float[][] mat1, float[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por multiplicar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public double[][] multiplicaMatrizes(double[][] mat1, double[][] mat2) throws MatrizException;
}