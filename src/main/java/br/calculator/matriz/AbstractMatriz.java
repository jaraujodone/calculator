/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 */
public abstract class AbstractMatriz {

	/**
	 * @param mat1
	 * @throws MatrizException
	 */
	protected void validarMatriz(final int[][] mat1) throws MatrizException {
		if(mat1 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatriz(final int[][] mat1, final int[][] mat2) throws MatrizException {
		if(mat1 == null || mat2 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0 || mat2.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatrizTamanho(final int[][] mat1, final int[][] mat2) throws MatrizException {
		validarMatriz(mat1, mat2);
		
		if(mat1.length != mat2.length || mat1[0].length != mat2[0].length) {
			throw new MatrizException("Matrizes nao pode ter de tamanho diferente");
		}
	}
	
	/**
	 * @param mat1
	 * @throws MatrizException
	 */
	protected void validarMatriz(final long[][] mat1) throws MatrizException {
		if(mat1 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatriz(final long[][] mat1, final long[][] mat2) throws MatrizException {
		if(mat1 == null || mat2 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0 || mat2.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatrizTamanho(final long[][] mat1, final long[][] mat2) throws MatrizException {
		validarMatriz(mat1, mat2);
		
		if(mat1.length != mat2.length || mat1[0].length != mat2[0].length) {
			throw new MatrizException("Matrizes nao pode ter de tamanho diferente");
		}
	}
	
	/**
	 * @param mat1
	 * @throws MatrizException
	 */
	protected void validarMatriz(final float[][] mat1) throws MatrizException {
		if(mat1 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatriz(final float[][] mat1, final float[][] mat2) throws MatrizException {
		if(mat1 == null || mat2 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0 || mat2.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatrizTamanho(final float[][] mat1, final float[][] mat2) throws MatrizException {
		validarMatriz(mat1, mat2);
		
		if(mat1.length != mat2.length || mat1[0].length != mat2[0].length) {
			throw new MatrizException("Matrizes nao pode ter de tamanho diferente");
		}
	}
	
	/**
	 * @param mat1
	 * @throws MatrizException
	 */
	protected void validarMatriz(final double[][] mat1) throws MatrizException {
		if(mat1 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatriz(final double[][] mat1, final double[][] mat2) throws MatrizException {
		if(mat1 == null || mat2 == null) {
			throw new MatrizException("Matriz nao pode ser nula");
		}
		
		if(mat1.length == 0 || mat2.length == 0) {
			throw new MatrizException("Matriz nao pode ter tamanho igual a zero");
		}
	}
	
	/**
	 * @param mat1
	 * @param mat2
	 * @throws MatrizException
	 */
	protected void validarMatrizTamanho(final double[][] mat1, final double[][] mat2) throws MatrizException {
		validarMatriz(mat1, mat2);
		
		if(mat1.length != mat2.length || mat1[0].length != mat2[0].length) {
			throw new MatrizException("Matrizes nao pode ter de tamanho diferente");
		}
	}
}