package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public class SomaMatriz extends AbstractMatriz implements ISomaMatriz {

	@Override
	public int[][] somaMatriz(int[][] mat1, int[][] mat2) throws MatrizException {

		validarMatrizTamanho(mat1, mat2);

		int[][] resultado = new int[mat1.length][mat1[0].length];

		for (int i = 0; i < mat2.length; i++) {
			for (int j = 0; j < mat2[0].length; j++) {
				resultado[i][j] = mat1[i][j] + mat2[i][j];
			}
		}

		return resultado;
	}

	@Override
	public long[][] somaMatriz(long[][] mat1, long[][] mat2) throws MatrizException {

		validarMatrizTamanho(mat1, mat2);

		long[][] resultado = new long[mat1.length][mat1[0].length];

		for (int i = 0; i < mat2.length; i++) {
			for (int j = 0; j < mat2[0].length; j++) {
				resultado[i][j] = mat1[i][j] + mat2[i][j];
			}
		}

		return resultado;
	}
	
	@Override
	public float[][] somaMatriz(float[][] mat1, float[][] mat2) throws MatrizException {

		validarMatrizTamanho(mat1, mat2);

		float[][] resultado = new float[mat1.length][mat1[0].length];

		for (int i = 0; i < mat2.length; i++) {
			for (int j = 0; j < mat2[0].length; j++) {
				resultado[i][j] = mat1[i][j] + mat2[i][j];
			}
		}

		return resultado;
	}
	
	@Override
	public double[][] somaMatriz(double[][] mat1, double[][] mat2) throws MatrizException {

		validarMatrizTamanho(mat1, mat2);

		double[][] resultado = new double[mat1.length][mat1[0].length];

		for (int i = 0; i < mat2.length; i++) {
			for (int j = 0; j < mat2[0].length; j++) {
				resultado[i][j] = mat1[i][j] + mat2[i][j];
			}
		}

		return resultado;
	}
}