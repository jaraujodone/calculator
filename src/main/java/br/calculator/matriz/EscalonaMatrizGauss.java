/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 *  
 * @since Calculator API 17.0
 */
public class EscalonaMatrizGauss implements IEscalonaMatrizGauss {

	@Override
	public float[][] escalonaMatrizGauss(int[][] matriz) throws MatrizException {

		if (matriz.length != matriz[0].length) {
			throw new MatrizException("N�o � possivel escalonar matriz n�o quadrada");
		}

		float[][] resultado = new float[matriz.length][matriz[0].length];

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				resultado[i][j] = matriz[i][j];
			}
		}

		int linha = 1, coluna = 0, colunaNova= 0;
		float pivo = 0, pivoSalvo = 0;

		while (linha < matriz.length) {

			coluna = colunaNova;

			if(linha == 1) {
				pivo = matriz[linha][0] / matriz[linha - 1][coluna];
				pivo *= -1;
				pivoSalvo = pivo;
			} else {
				pivo = matriz[linha][0] / pivoSalvo;
				pivoSalvo = pivo;
			}

			coluna = 0;

			while (coluna < matriz[0].length) {
				resultado[linha][coluna] = (pivo * matriz[0][coluna]) + matriz[linha][coluna];
				coluna++;
			}

			colunaNova++;
			linha++;
		}

		return resultado;
	}
}