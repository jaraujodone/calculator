/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 15.0
 */
public class MatrizOposta extends AbstractMatriz implements IMatrizOposta {

	@Override
	public int[][] matrizOposta(int[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		int[][] matrizOposta = new int[matriz.length][matriz[0].length];

		for (int linha = 0; linha < matrizOposta.length; linha++) {
			for (int coluna = 0; coluna < matrizOposta[0].length; coluna++) {
				matrizOposta[linha][coluna] = -1 * matriz[linha][coluna];
			}
		}

		return matrizOposta;
	}

	@Override
	public long[][] matrizOposta(long[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		long[][] matrizOposta = new long[matriz.length][matriz[0].length];

		for (int linha = 0; linha < matrizOposta.length; linha++) {
			for (int coluna = 0; coluna < matrizOposta[0].length; coluna++) {
				matrizOposta[linha][coluna] = -1 * matriz[linha][coluna];
			}
		}

		return matrizOposta;
	}

	@Override
	public float[][] matrizOposta(float[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		float[][] matrizOposta = new float[matriz.length][matriz[0].length];

		for (int linha = 0; linha < matrizOposta.length; linha++) {
			for (int coluna = 0; coluna < matrizOposta[0].length; coluna++) {
				matrizOposta[linha][coluna] = -1 * matriz[linha][coluna];
			}
		}

		return matrizOposta;
	}

	@Override
	public double[][] matrizOposta(double[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		double[][] matrizOposta = new double[matriz.length][matriz[0].length];

		for (int linha = 0; linha < matrizOposta.length; linha++) {
			for (int coluna = 0; coluna < matrizOposta[0].length; coluna++) {
				matrizOposta[linha][coluna] = -1 * matriz[linha][coluna];
			}
		}

		return matrizOposta;
	}
}