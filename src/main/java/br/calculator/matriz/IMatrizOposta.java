/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IMatrizOposta {
	
	/**
	 * Metodo responsavel por calcular a matriz oposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public int[][] matrizOposta(final int[][] matriz) throws MatrizException;

	/**
	 * Metodo responsavel por calcular a matriz oposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public long[][] matrizOposta(final long[][] matriz) throws MatrizException;

	/**
	 * Metodo responsavel por calcular a matriz oposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public float[][] matrizOposta(final float[][] matriz) throws MatrizException;

	/**
	 * Metodo responsavel por calcular a matriz oposta
	 * 
	 * @param matriz
	 * @return
	 * @throws MatrizException 
	 */
	public double[][] matrizOposta(final double[][] matriz) throws MatrizException;
}