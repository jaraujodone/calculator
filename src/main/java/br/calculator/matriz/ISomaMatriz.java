package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface ISomaMatriz {
	
	/**
	 * Metodo responsavel por somar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public int[][] somaMatriz(int[][] mat1, int[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por somar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public long[][] somaMatriz(long[][] mat1, long[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por somar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public float[][] somaMatriz(float[][] mat1, float[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por somar duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public double[][] somaMatriz(double[][] mat1, double[][] mat2) throws MatrizException;
}