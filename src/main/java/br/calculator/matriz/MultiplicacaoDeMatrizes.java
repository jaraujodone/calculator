/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public class MultiplicacaoDeMatrizes extends AbstractMatriz implements IMultiplicacaoDeMatrizes {

	@Override
	public int[][] multiplicaMatrizes(int[][] mat1, int[][] mat2) throws MatrizException {
		
		validarMatriz(mat1, mat2);
		
		if(mat1[0].length != mat2.length || mat1 == null || mat2 == null) {
			throw new MatrizException("N�o � possivel Multiplicar Matriz " 
					+ mat1.length + "X" + mat1[0].length + " por uma " + mat2.length + "X" + mat2[0].length);
		}
		
		int[][] matResultado = new int[mat1.length][mat2[0].length];
		int acumula = 0;
		
		for (int linha = 0; linha < mat1.length; linha++) {
			for (int coluna = 0; coluna < mat2[0].length; coluna++) {
				acumula = 0;
				
				for (int i = 0; i < mat1[0].length; i++) {
					acumula += mat1[linha][i] * mat2[i][coluna];
				}
				
				matResultado[linha][coluna] = acumula;
			}
		}
		
		return matResultado;
	}
	
	@Override
	public long[][] multiplicaMatrizes(long[][] mat1, long[][] mat2) throws MatrizException {
		
		validarMatriz(mat1, mat2);
		
		if(mat1[0].length != mat2.length || mat1 == null || mat2 == null) {
			throw new MatrizException("N�o � possivel Multiplicar Matriz " 
					+ mat1.length + "X" + mat1[0].length + " por uma " + mat2.length + "X" + mat2[0].length);
		}
		
		long[][] matResultado = new long[mat1.length][mat2[0].length];
		long acumula = 0;
		
		for (int linha = 0; linha < mat1.length; linha++) {
			for (int coluna = 0; coluna < mat2[0].length; coluna++) {
				acumula = 0;
				
				for (int i = 0; i < mat1[0].length; i++) {
					acumula += mat1[linha][i] * mat2[i][coluna];
				}
				
				matResultado[linha][coluna] = acumula;
			}
		}
		
		return matResultado;
	}
	
	@Override
	public float[][] multiplicaMatrizes(float[][] mat1, float[][] mat2) throws MatrizException {
		
		validarMatriz(mat1, mat2);
		
		if(mat1[0].length != mat2.length || mat1 == null || mat2 == null) {
			throw new MatrizException("N�o � possivel Multiplicar Matriz " 
					+ mat1.length + "X" + mat1[0].length + " por uma " + mat2.length + "X" + mat2[0].length);
		}
		
		float[][] matResultado = new float[mat1.length][mat2[0].length];
		float acumula = 0;
		
		for (int linha = 0; linha < mat1.length; linha++) {
			for (int coluna = 0; coluna < mat2[0].length; coluna++) {
				acumula = 0;
				
				for (int i = 0; i < mat1[0].length; i++) {
					acumula += mat1[linha][i] * mat2[i][coluna];
				}
				
				matResultado[linha][coluna] = acumula;
			}
		}
		
		return matResultado;
	}
	
	@Override
	public double[][] multiplicaMatrizes(double[][] mat1, double[][] mat2) throws MatrizException {
		
		validarMatriz(mat1, mat2);
		
		if(mat1[0].length != mat2.length || mat1 == null || mat2 == null) {
			throw new MatrizException("N�o � possivel Multiplicar Matriz " 
					+ mat1.length + "X" + mat1[0].length + " por uma " + mat2.length + "X" + mat2[0].length);
		}
		
		double[][] matResultado = new double[mat1.length][mat2[0].length];
		double acumula = 0;
		
		for (int linha = 0; linha < mat1.length; linha++) {
			for (int coluna = 0; coluna < mat2[0].length; coluna++) {
				acumula = 0;
				
				for (int i = 0; i < mat1[0].length; i++) {
					acumula += mat1[linha][i] * mat2[i][coluna];
				}
				
				matResultado[linha][coluna] = acumula;
			}
		}
		
		return matResultado;
	}
}