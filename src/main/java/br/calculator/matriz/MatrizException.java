/**
 * 
 */
package br.calculator.matriz;

/**
 * @author Jesse A. Done
 */
public class MatrizException extends Exception {
	private static final long serialVersionUID = 2912749723620355105L;

	/**
	 * @param paramString
	 */
	public MatrizException(final String paramString) {
		super(paramString);
	}

	/**
	 * @param paramString
	 * @param paramThrowable
	 */
	public MatrizException(final String paramString, final Throwable paramThrowable) {
		super(paramString, paramThrowable);
	}
}