package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface ISubtrairMatriz {
	
	/**
	 * Metodo responsavel por subtrair duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public int[][] subtrairMatriz(int[][] mat1, int[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por subtrair duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public long[][] subtrairMatriz(long[][] mat1, long[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por subtrair duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public float[][] subtrairMatriz(float[][] mat1, float[][] mat2) throws MatrizException;
	
	/**
	 * Metodo responsavel por subtrair duas matrizes
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public double[][] subtrairMatriz(double[][] mat1, double[][] mat2) throws MatrizException;
}