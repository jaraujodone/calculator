package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public class MultiplicaMatrizPorEscalar extends AbstractMatriz implements IMultiplicaMatrizPorEscalar {

	@Override
	public int[][] multiplicaMatrizPorEscalar(int escalar, int[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		int[][] resultado = new int[matriz.length][matriz[0].length];
		
		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[0].length; j++) {
				resultado[i][j] = escalar * matriz[i][j];
			}
		}
		
		return resultado;
	}
	
	@Override
	public long[][] multiplicaMatrizPorEscalar(long escalar, long[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		long[][] resultado = new long[matriz.length][matriz[0].length];
		
		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[0].length; j++) {
				resultado[i][j] = escalar * matriz[i][j];
			}
		}
		
		return resultado;
	}
	
	@Override
	public long[][] multiplicaMatrizPorEscalar(int escalar, long[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		long[][] resultado = new long[matriz.length][matriz[0].length];
		
		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[0].length; j++) {
				resultado[i][j] = escalar * matriz[i][j];
			}
		}
		
		return resultado;
	}
	
	@Override
	public float[][] multiplicaMatrizPorEscalar(float escalar, float[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		float[][] resultado = new float[matriz.length][matriz[0].length];
		
		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[0].length; j++) {
				resultado[i][j] = escalar * matriz[i][j];
			}
		}
		
		return resultado;
	}
	
	@Override
	public double[][] multiplicaMatrizPorEscalar(double escalar, double[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		double[][] resultado = new double[matriz.length][matriz[0].length];
		
		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[0].length; j++) {
				resultado[i][j] = escalar * matriz[i][j];
			}
		}
		
		return resultado;
	}
	
	@Override
	public float[][] multiplicaMatrizPorEscalar(int escalar, float[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		float[][] resultado = new float[matriz.length][matriz[0].length];
		
		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[0].length; j++) {
				resultado[i][j] = escalar * matriz[i][j];
			}
		}
		
		return resultado;
	}
	
	@Override
	public double[][] multiplicaMatrizPorEscalar(int escalar, double[][] matriz) throws MatrizException {
		validarMatriz(matriz);
		
		double[][] resultado = new double[matriz.length][matriz[0].length];
		
		for (int i = 0; i < resultado.length; i++) {
			for (int j = 0; j < resultado[0].length; j++) {
				resultado[i][j] = escalar * matriz[i][j];
			}
		}
		
		return resultado;
	}
}