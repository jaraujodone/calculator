package br.calculator.matriz;

/**
 * @author Jesse A. Done
 * 
 * @since Calculator API 1.0
 */
public interface IVerificaMatrizInversa {
	
	/**
	 * Metodo responsavel por verificar se as matrizes sao inversa
	 * retorna true se matrizes inversas
	 * retorna falso se matrizes nao forem inversas
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public boolean verificaMatrizInversa(int[][] mat1, int[][] mat2) throws MatrizException;

	/**
	 * Metodo responsavel por verificar se as matrizes sao inversa
	 * retorna true se matrizes inversas
	 * retorna falso se matrizes nao forem inversas
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public boolean verificaMatrizInversa(long[][] mat1, long[][] mat2) throws MatrizException;

	/**
	 * Metodo responsavel por verificar se as matrizes sao inversa
	 * retorna true se matrizes inversas
	 * retorna falso se matrizes nao forem inversas
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public boolean verificaMatrizInversa(float[][] mat1, float[][] mat2) throws MatrizException;

	/**
	 * Metodo responsavel por verificar se as matrizes sao inversa
	 * retorna true se matrizes inversas
	 * retorna falso se matrizes nao forem inversas
	 * 
	 * @param mat1
	 * @param mat2
	 * @return
	 * @throws MatrizException
	 */
	public boolean verificaMatrizInversa(double[][] mat1, double[][] mat2) throws MatrizException;
}